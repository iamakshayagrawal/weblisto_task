from django.db import models
from django.contrib.auth.models import User

class Product(models.Model):
	name = models.CharField(max_length=100)
	price = models.IntegerField()
	description = models.CharField(max_length=100)
	use = models.CharField(max_length=100)
	is_indian_product = models.BooleanField(default=False)
	categories = models.ManyToManyField('Category')
	tag_names = models.ManyToManyField('Tag')

	def __str__(self):
		return self.name

class Category(models.Model):
	category_name = models.CharField(max_length=100)

	def __str__(self):
		return self.category_name

class Tag(models.Model):
	tag_name = models.CharField(max_length=100)

	def __str__(self):
		return self.tag_name