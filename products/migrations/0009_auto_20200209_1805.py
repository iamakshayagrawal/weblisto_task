# Generated by Django 2.2 on 2020-02-09 18:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0008_auto_20200209_1804'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='tag_names',
            field=models.ManyToManyField(to='products.Tag'),
        ),
        migrations.AlterField(
            model_name='product',
            name='categories',
            field=models.ManyToManyField(to='products.Category'),
        ),
    ]
