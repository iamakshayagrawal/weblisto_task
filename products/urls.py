from django.urls import path
from .views import *

urlpatterns = [
	path('', ProductListView.as_view(), name='home'),
	path('add-product/', AddProductView.as_view(), name='add_product'),
	path('view-product/<int:pk>/', ViewProductView.as_view(), name='view_product'),
	path('edit-product/<int:pk>/', EditProductView.as_view(), name='edit_product'),
	path('delete-product/<int:pk>/', DeleteProductView.as_view(), name='delete_product'),
]