from django import forms
from . models import *

class Product(forms.ModelForm):
	class Meta:
		model = Product, Category, Tag
		fields = '__all__'