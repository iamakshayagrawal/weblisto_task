from django.shortcuts import render, HttpResponse
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import DetailView, ListView
from . models import *

class ProductListView(ListView):
	model = Product
	context_object_name = 'all_products'
	template_name = 'all_products.html'

class AddProductView(CreateView):
	model = Product
	fields = '__all__'
	template_name = 'add_product.html'
	success_url = '/'

class ViewProductView(DetailView):
	model = Product
	context_object_name = 'item'
	fields = '__all__'
	template_name = 'view_product.html'

class EditProductView(UpdateView):
	model = Product
	fields = '__all__'
	template_name = 'edit_product.html'
	success_url = '/'

class DeleteProductView(DeleteView):
	model = Product
	template_name = 'product_confirm_delete.html'
	success_url = '/'