from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password


class SignUpForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput())
	confirm_password = forms.CharField(widget=forms.PasswordInput())
	email = forms.EmailField(required=True)
	class Meta:
		model = User
		fields = ('username', 'first_name', 'last_name', 'email', 'password',)

	def clean_username(self):
		username = self.cleaned_data['username']
		user = User.objects.filter(username=username)
		if user.exists():
			raise forms.ValidationError('user already exits')
	
		return username

	def clean_email(self):
		email = self.cleaned_data['email']
		user = User.objects.filter(email=email)
		if user.exists():
			raise forms.ValidationError('email already exits')
	
		return email

	def clean_confirm_password(self):
		password = self.cleaned_data['password']
		confirm_password = self.cleaned_data['confirm_password']
		if password != confirm_password:
			raise forms.ValidationError('password and confirm password do not match')
		return confirm_password

class LoginForm(forms.Form):
	username = forms.CharField()
	password = forms.CharField(widget=forms.PasswordInput)

	def clean(self):		
		username = self.cleaned_data['username']
		password = self.cleaned_data['password']
		if User.objects.filter(username = username).exists():
			user = User.objects.filter(username = username)[0]
			valid_password = user.check_password(password)
			if not valid_password:
				raise forms.ValidationError('Entered wrong password')
			return self.cleaned_data
		else :
			raise forms.ValidationError('Not a known user')