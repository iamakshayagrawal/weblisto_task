from django.shortcuts import render, reverse, redirect
from django.views import View
from .forms import SignUpForm, LoginForm
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required


class SignUpView(View):

	def get(self, request, *args, **kwargs):
		form = SignUpForm()
		return render(request, 'signup.html', {'form':form})

	def post(self, request, *args, **kwargs):
		form = SignUpForm(request.POST)
		if form.is_valid():
			cleaned_data = form.cleaned_data
			username = cleaned_data['username']
			first_name = cleaned_data['first_name']
			last_name = cleaned_data['last_name']
			email = cleaned_data['email']
			password = cleaned_data['password']

			User.objects.create(username=username, first_name=first_name, last_name=last_name, email=email, password=password)

		return render(request, 'signup.html', {'form':form})

class LoginView(View):

	def get(elf, request, *args, **kwargs):
		form = LoginForm()
		return render(request, 'login.html', {'form':form})

	def post(self, request, *args, **kwargs):
		form = LoginForm(request.POST)
		if form.is_valid():
			cleaned_data = form.cleaned_data
			username = cleaned_data['username']
			password = cleaned_data['password']

			user = authenticate(request, username=username, password=password)
			
			if user is not None :
				if user.is_active:
					login(request, user)
					return redirect(reverse('home'))
		else:			
			return render(request, 'login.html', {'form':form})

@login_required
def logout_view(request):
	logout(request)
	return redirect(reverse('home'))